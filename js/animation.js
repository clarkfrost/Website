var colors = [];
var count = 10;
var radius = 25;
var buffer = 25;

function rgb(r, g, b) {
    return "rgb(" + r + "," + g + "," + b + ")";
}

function hsv_to_rgb(h, s, v) {
    var r, g, b, i, f, p, q, t;
    if (arguments.length === 1) {
        s = h.s, v = h.v, h = h.h;
    }
    i = Math.floor(h * 6);
    f = h * 6 - i;
    p = v * (1 - s);
    q = v * (1 - f * s);
    t = v * (1 - (1 - f) * s);
    switch (i % 6) {
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
    }
    return {
        r: Math.round(r * 255),
        g: Math.round(g * 255),
        b: Math.round(b * 255)
    };
}

function getRandomColor() {
    var golden_ration_conjugate = 0.618033988749895;
    var h = Math.random();

    h += golden_ration_conjugate;
    h %= 1;
    
    return hsv_to_rgb(h, 0.5, 0.95);
}

function init() {
    document.getElementById('animation').width = $(window).width();
    document.getElementById('animation').height = 100;

    for(var i = 0; i < count; i++) {
        var color = getRandomColor();
        var rgbColor = rgb(color.r, color.g, color.b);

        colors.push(rgbColor);
    }

    window.setInterval(draw, 500);
}

function draw() {
    var ctx = document.getElementById('animation').getContext('2d');

    var width = $(window).width();
    var height = document.getElementById('animation').height;

    ctx.clearRect(0, 0, width, height);

    var bufferSpace = ((count / 2) - 1) * buffer;
    var circleSpace = (count / 2) * 2 * radius;
    var x = ($(document).width() / 2) - bufferSpace - circleSpace;

    console.log(width);

    colors.forEach(function(item, index, array) {
        ctx.beginPath();
        ctx.arc(x, radius + 1, radius, 0, 2 * Math.PI);
        ctx.fillStyle = item;
        ctx.fill();
        ctx.strokeStyle = item;
        ctx.stroke();

        x += (2 * radius) + buffer;
    });

    var color = getRandomColor();
    var rgbColor = rgb(color.r, color.g, color.b);

    colors.unshift(rgbColor);
    colors.splice(colors.length - 1, 1);
}

init();
